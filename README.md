# README #

1/3スケールのカシオ FP-1000/1100風小物のstlファイルです。
スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。
元ファイルはAUTODESK 123D DESIGNです。


***

# 実機情報

## メーカ
- カシオ

## 発売時期
- 1982年

## 参考資料

- [Wikipedia](https://ja.wikipedia.org/wiki/FP-1000)

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_fp-1000/raw/ebbacda0d2db2d2b7ce4ed3e9fac79e97e68a0c0/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fp-1000/raw/ebbacda0d2db2d2b7ce4ed3e9fac79e97e68a0c0/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_fp-1000/raw/ebbacda0d2db2d2b7ce4ed3e9fac79e97e68a0c0/ExampleImage.jpg)
